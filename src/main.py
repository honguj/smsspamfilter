from databaseConnection import *
from BayesianFilter import *
from SvmFilter import *
from decimal import *
import MySQLdb

THRES_HOLD = 0.98
SPAM = 1
HAM = 0

dbCon = DatabaseConnection()

# Test database connection
#result = dbCon.searchQuery("SELECT * FROM messages WHERE type = %s", ('ham'))
#result = dbCon.getTestData('spam')
#for row in result:
#	print row


# For bayesian algorithm
print "BAYESIAN ALGORITHM:"
bayesian = Bayesian(dbCon.getTrainingData('spam'), dbCon.getTrainingData('ham'))
bayesian.trainData()
bayesian.test(dbCon.getTestData('spam'), dbCon.getTestData('ham'), THRES_HOLD)

# Write the list of all tokens to external files - Used for debugging purposes
log = open('Tokens.txt','w')
for key in bayesian.spamTokens.keys():
	log.write(key + "-" + str(bayesian.spamTokens[key]) + "\n")
log.close()

log = open('WordProba.txt','w')
for key in bayesian.wordProba.keys():
	log.write(key + "-" + str(bayesian.wordProba[key]) + "\n")
log.close()

print "-----------------------------------------------------------------"

# SVM algorithm
print "SVM ALGORITHM:"
svmFilter = SvmFilter(dbCon.getTrainingData('spam'), dbCon.getTrainingData('ham'))
svmFilter.trainData(True) # Pass True as parameter if we want to load the precomputed knowledge base from file smsSpamFilter.model.
							# Pass False if we want to re-generate the knowledge base of the Filter

# Get test data
spamTestData = list( (row[2]) for row in dbCon.getTestData('spam') )
hamTestData = list( (row[2]) for row in dbCon.getTestData('ham') )

print "Spam test: "
p_labels, p_acc, p_vals = svmFilter.predict(spamTestData, SPAM)

print "Ham test: "
p_labels, p_acc, p_vals = svmFilter.predict(hamTestData, HAM)