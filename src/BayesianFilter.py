from Tokenizer import *
from decimal import *

class Bayesian:
	def __init__(self, spamCollection, hamCollection):
		self.spamMessages = []
		for s in spamCollection:
			self.spamMessages.append(s[2])
		self.hamMessages = []
		for h in hamCollection:
			self.hamMessages.append(h[2])
		self.spamTokens = Tokenizer.getTokens(self.spamMessages)
		self.hamTokens = Tokenizer.getTokens(self.hamMessages)
		self.wordProba = {}

	def trainData(self):
		# set float precision to 20
		getcontext().prec = 20
		
		# Only use word which occurs more than 2 times in spams dataset
		spamTokens = filter(lambda word: self.spamTokens[word] > 2 and word in self.hamTokens,self.spamTokens.keys())

		for word in spamTokens:
			numerator = min(1, Decimal(self.spamTokens[word])/Decimal(len(self.spamMessages)))
			if word in self.hamTokens:	
				denominator = numerator + min(1.0, Decimal(self.hamTokens[word])/Decimal(len(self.hamMessages)))
			self.wordProba[word] = numerator/denominator


	# This function is used to get the probability of a message which is detected as a spam message
	def calculateSpamProba(self, message):
		#log = open('Debug.txt','w')
		getcontext().prec = 20
		messageTokens = Tokenizer.getTokens([message])
		#print messageTokens
		numerator = Decimal(1)
		denominator = Decimal(1)
		for word in messageTokens.keys():
			if word in self.wordProba:
		#		log.write(word + ": " + str(self.wordProba[word]) + "\n")
				numerator *= self.wordProba[word]
				denominator *= (Decimal(1) - self.wordProba[word])
		#log.write('=' + str(numerator) + "/" + str(denominator))		
		denominator += numerator
		#log.write('=' + str(numerator) + "/" + str(denominator))		
		#log.close()
		return numerator/denominator
	

	# This function is used to test the bayesian algorithm with a list a spamTestData and hamTestData
	def test(self, spamTestData, hamTestData, Threshold):
		getcontext().prec = 20

		# Test all spam messages in test dataset
		spamTest = {}
		falsePositive = [] # False positive is a spam message which is detected as a ham message
		for row in spamTestData:
			p = self.calculateSpamProba(row[2])
			spamTest[row[2]] = p
			if p < Threshold:
				falsePositive.append((row, p))
		print "Spam test - Accuracy: ", Decimal(len(spamTestData) - len(falsePositive))/Decimal(len(spamTestData)), "(", len(spamTestData) - len(falsePositive), "/", len(spamTestData), ")"

		# Write to file all the false positive - Spam messages which are detected as ham messages
		log = open('FalsePositive.txt','w')
		log.write('Spam messages which are detected as ham messages: ')
		log.write('\n')
		for p in falsePositive:
			log.write(str(p[0][0]) + "-" + str(p[1]) + "-" + p[0][2] + '\n')
		log.close()

		# Test all ham messages in test dataset
		hamTest = {}
		falseNegative = [] # False negative is a ham message which is detected as a spam message
		for row in hamTestData:
			p = self.calculateSpamProba(row[2])
			hamTest[row[2]] = p
			if p > Threshold:
				falseNegative.append((row, p))
		print "Ham test - Accuracy: ", Decimal(len(hamTestData) - len(falseNegative))/Decimal(len(hamTestData)), "(", len(hamTestData) - len(falseNegative), "/", len(hamTestData), ")"

		# Write to file all the false negative - Ham messages which are detected as spam messages
		log = open('FalseNegative.txt','w')
		log.write('Ham messages which are detected as spam messages: ')
		log.write('\n')
		for p in falseNegative:
			log.write(str(p[0][0]) + "-" + str(p[1]) + "-" + p[0][2] + '\n')
		log.close()