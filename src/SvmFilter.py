from Tokenizer import *
from decimal import *
from svm import *
from svmutil import *

class SvmFilter:
	def __init__(self, spamCollection, hamCollection):
		self.spamMessages = []
		for s in spamCollection:
			self.spamMessages.append(s[2])
		self.hamMessages = []
		for h in hamCollection:
			self.hamMessages.append(h[2])
		self.spamTokens = Tokenizer.getTokens(self.spamMessages)
		self.hamTokens = Tokenizer.getTokens(self.hamMessages)
		self.topTokens = {}

		# Build topTokens dictionary which contains the most frequent tokens used in all messages in trainingSet
		self.topTokens = dict( (n, self.hamTokens.get(n, 0) + self.spamTokens.get(n, 0) ) for n in set(self.hamTokens)|set(self.spamTokens) )
		
		# List of top words
		self.topWords = sorted(self.topTokens.iteritems(), key=operator.itemgetter(1), reverse=True)
		self.topWords = list( (p[0]) for p in self.topWords)

		self.spamSampleFeatures = [] # list of spam message features following the libsvm format
		self.hamSampleFeatures = []
		self.model = None

	def trainData(self, loadFromFile=False):
		if loadFromFile:
			self.model = svm_load_model("smsSpamFilter.model")
		else:
			# Train data for spam messages
			for message in self.spamMessages:
				messageTokens = Tokenizer.getTokens([message])
				messageFeatures = list( (messageTokens.get(w, 0)) for w in self.topWords )
				self.spamSampleFeatures.append(messageFeatures)

			# Train data for ham messages
			for message in self.hamMessages:
				messageTokens = Tokenizer.getTokens([message])
				messageFeatures = list( (messageTokens.get(w, 0)) for w in self.topWords )
				self.hamSampleFeatures.append(messageFeatures)			

			# Declare svm problem using libSVM
			samples = self.spamSampleFeatures + self.hamSampleFeatures
			labels = [1] * len(self.spamSampleFeatures) + [0] * len(self.hamSampleFeatures )
			
			problem = svm_problem(labels, samples)
			param = svm_parameter('-t 0 -c 50 -b 1')
			# http://classes.soe.ucsc.edu/cmps290c/Spring12/lect/14/00788645-SVMspam.pdf => cost factor C > 50 to have better performance
			# http://www.interscience.in/SpIss_ijcct_accta_2010vol1_nol2/CS_Paper8.pdf
			self.model = svm_train(problem, param)
			svm_save_model('smsSpamFilter.model', self.model)

	def predict(self, messages, expectedValue):
		# build feature vector of this message
		messagesToPredict = []
		for message in messages:
			messageTokens = Tokenizer.getTokens([message])
			messageFeatures = list( (messageTokens.get(w, 0)) for w in self.topWords )
			messageFeaturesDict = dict( (x[0], x[1]) for x in zip(range(len(messageFeatures)), messageFeatures) )
			messagesToPredict.append(messageFeaturesDict)

		p_labels, p_acc, p_vals = svm_predict([expectedValue]*len(messagesToPredict), messagesToPredict, self.model) # First parameter is a list of labels, second parameter is a list of feature dictionary
		return p_labels, p_acc, p_vals



