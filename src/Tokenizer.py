import re
import operator

class Tokenizer:

	@staticmethod
	def isPhoneNumber(word):
		num_format = re.compile("[0-9]{7,}")
		m = re.match(num_format,word)
		if m:
			return True
		else:
			return False

	# Return a dictionary which maps each character with their frequence in the collection
	@staticmethod
	def getTokens(messageCollection):
		tokens = {}
	
		for message in messageCollection:
			# Split message into words
			elems = re.split(r'[!?<>,.:;|() ]+', message)
			for e in elems:
				# Remove separators
				e = re.sub(r'"[.,:\n]+[ \n]{1,}"', '', e)
				e = e.replace('\n', '')
				e = e.replace("'s","")

				# Verify if e is a phone number
				if Tokenizer.isPhoneNumber(e):
					e = 'phone_number'	

				# Save to hash map
				if e.lower() in tokens:
					tokens[e.lower()] += 1
				else:
					tokens[e.lower()] = 1
		return tokens
