#!/usr/bin/python
import MySQLdb

class DatabaseConnection:
	def __init__(self):
		dbConfigFname = "./../database/dbConfig"
		ins = open(dbConfigFname, "r")
		dbConfigDict = {}
		for line in ins:
			tmp = line.split(' ')
			dbConfigDict[tmp[0]] = tmp[1][:-1]
		self.host = dbConfigDict['host']
		self.user = dbConfigDict['user']
		self.passwd = dbConfigDict['passwd']
		self.db = dbConfigDict['db']
		self.db = MySQLdb.connect(self.host, # your host, usually localhost
                		self.user, # your username
                      	self.passwd, # your password
                      	self.db) # name of the data base


	def searchQuery(self, query, data):
		cur = self.db.cursor() 
		cur.execute(query, data)

		return cur.fetchall()

	# Used to insert new message into database
	def insertQuery(self, query, data):
		cur = self.db.cursor()
		cur.execute(query, data)
		self.db.commit()
		cur.close()

	# We divide our data into two sets: training data and test data
	# getTrainingData and getTestData will give us 2 different sets of data
	def getTrainingData(self, dataType):
		count = 0
		if dataType == 'spam':
			count = 498
		elif dataType == 'ham':
			count = 3218
		result = self.searchQuery("SELECT * FROM messages WHERE type= %s ORDER BY id LIMIT %s",(dataType, count))

		return result		


	def getTestData(self, dataType):
		count = 0
		if dataType == 'spam':
			count = 373
		elif dataType == 'ham':
			count = 1609
		return self.searchQuery("SELECT * FROM messages WHERE type= %s ORDER BY id DESC LIMIT %s",(dataType, count))

	def getData(self, id):
		return self.searchQuery("SELECT * FROM messages WHERE id=%s", id)

	def __del__(self):
		self.db.close()
        	print 'Closed dbConnection'

