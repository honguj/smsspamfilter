INSTALL DATABASE (mySql)
To install database, you need to:
	1) Create database by executing all commands found in database/dbChanges.sql
	2) Import the database from database/dbCopy.sql

You need to add your database configuration to the project:
	1) Go to database/ folder:
		cd database/
	2) Copy the template for dbConfig:
		cp dbConfig.tmp dbConfig
	3) dbConfig is where you have to put your database informations. This file will be ignored by Git.




